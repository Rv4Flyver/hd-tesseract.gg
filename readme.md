## TO DO (prototype level)
    - mutators system
## done
    - custom weapons
    - custom pickups
    - custom movement

___________________________________________________________________________________________________
# Info

## What is the coordinate system used by tesseract?
>   Z up left handed

## CTF Flag
    // called on pickup
    void takeflag(clientinfo *ci, int i, int version)
    void ownflag(int i, gameent *owner, int owntime) 

## Custom Entities
### src/game/entities.cpp
    #define validitem(n) (NOTUSED <= n && n < MAXENTTYPES)	  // TO DO  
    
>    static const char * const entmdlnames[MAXENTTYPES] =      // specify mdl path

>    static const char * const entnames[MAXENTTYPES] =         // specify ent name
    
>    void trypickup(int n, gameent *d)                         // pickup if entity nearby 
    
>    void pickupeffects(int n, gameent *d)                     // invokes pickup and related functionality on N_ITEMACC event
    
>    void pickup(int type)                                     // called by pickupeffects if can pickup entity

    // ??? ----------------------------------------------------------------------------------------------------
    static struct itemstat { int add, max, sound; const char *name; int icon, info; } itemstats[] =
    {
        { 10, 30, S_ITEMSPAWN, "CM", HICON_STEP,  CUSTOM},
    };

### src/game/game.cpp
>    void drawhudicons(gameent *d)                             // draw HUD icons (HEALTH & ammo)

### src/game/server.cpp
>    int spawntime(int type)                                   // return spawn time for entity of type








# physics.cpp

## Dodge against wall
    bool move(physent *d, vec &dir)
            ...
        	// Update Movement State
            if (d->movementState != MOVEMNT_NORMAL) {
                if (d->physstate == PHYS_FLOOR) {
                    d->movementState = MOVEMNT_NORMAL;
                }
            } // Update Movement State
            ...
            if (d->jumping && (fabs(obstacle.x) > 0 || fabs(obstacle.y) > 0) && d->movementState == MOVEMNT_NORMAL  && wallAttckAngle > 2.0f) {
                if (d->vel.magnitude() < d->maxspeed * 2.0f) {
                    bounceAgainstWall(d, dir, slide ? obstacle : floor, found, slidecollide);
                    d->movementState = MOVEMNT_WDODGED;
                }
            }
    void bounceAgainstWall(physent *d, vec &dir, const vec &obstacle, bool foundfloor, bool slidecollide)

## Jumping / BHOP

    void modifyvelocity(physent *pl, bool local, bool water, bool floating, int curtime)
        //pl->jumping = false; // prevent loop-jumping



___________________________________________________________________________________________________
# FUNCTIONS
### addmsg(N_SWITCHNAME, "rs", player1->name);
___________________________________________________________________________________________________
# ADDMSG (chasester responce on forum)

>   Q: addmsg is the way the buffer gets created for client/server interactions. Well chase how does this work?
    
    Basically you send a long byte data (1 and 0's) to the server (and from the server), this data is not formated, meaning the reciever has no clue what this data is or how it operates. This type of data is called a buffer.
    So how does the receiver read this message? Well simple, it assumes that the data the sender put is the same data the receiver is pulling out. So what happens if its not? you get a bad communication error, and you are instantly disconnected from the server (this rarely happens unless you screwed up your code).
    So what is addmsg and how does it work.
    
    So first parameter of addmsg is a N_ message type, basically this tells the reciever what kinda of data is being sent. The second parameter is the setup of this message:
    r - Reliable data    + adds the client number so the receiver knows who sent this data
    c - unreliable data + adds the client number so the receiver knows who sent this data
    v - vector (uses all the rest of the data in the buffer so should be defined last)
    i - int
    f - float
    s - string
    
    1-9 - repeat the previous one # times; so i5 is 5 ints but f5i is five floats and one int
    3-N parameters are pass in variables following the above formating.
    So if i were to create a new N_ message say to environmental damage a player (cuz you cant do that right now).
    
    I would first find the Client N_ enum and find a new fancy name like N_ENVIRO  I would add this in game.h in the two spots (probably at the end cuz why not).
    Then I would go to some were in the game.cpp file and add the functionality (like what to do when the functions is called) so something like:
   
>       void enviromentaldam(fpsent d, int dam, bool local)
>       {
>           damaged(dam, d, d, local) //from game.cpp  this does the effects, particles and changes the health for us
>           if(local) addmsg(N_ENVIRO, "ri" dam); //i'm not 100% on this look at damaged() its pretty simple to follow in there
>       }

    Now that we have done all the cool effects subtracted the damage and told the server, we need a reason to call this function 0.0 How about the player falling too far.
    There is a physics trigger that fires when the player falls from a far height (makes a thud instead of a pmp) let's use that to add damage.

>        void physicstrigger(...)
>        {
>        ...
>        else if(floorlevel<0) { if(d==player1 || d->type!=ENT_PLAYER || ((gameent *)d)->ai)
>            {
>            msgsound(S_LAND, d); 
>        //now lets add a check
>            if(d->timeinair > 1000) enviromentaldamage((timeinair -1000)*0.01, true);
>            }
>        }
    ok now we need to do the server side :)

    In void parsepacket() add a new N_ENVIRO parsemessage. Now the best way to set this up (and probably correct way) event called environment event (look at shotevent). Then just do the damage, check the validity of the call, and tell all the clients the new health using sendf(). Then check to see if the damaged player is dead, if so sendf() again to kill the player.
    WHy should you use events. LAG, basically a message is received with a "game Time" that they happen then the server determines what order those events should be triggered. Example: if a player is falling with one health, and if he hits the ground he will take 10 damage, but some one shoots a rocket at the ground under him. Does he die form the player or from the ground? Well the server hast to make this call based on the game time, and the position of the rocket/player (maybe even other elements. So setting up this into an event system will help improve accuracy immensely.
    If you have further question just ask, Ive done a lot of server/client modding so I understand most of it.
    chasester
    PS: As always this code is probably super buggy, Hopefully this proves the concept, you will need to do a lil work improving this code. (Also IDR if tesseract has a timeinair variables, if not you should add it to the physent struct as an int, and then increase it in moveplayer.
    PSS: Ive  done this environment damage before in sauerbraten so it is fairly easy, But I would add a new gun type called GUN_ENVIROMENT, so you can set a different message if they die from falling or burning or what ever other type of environment damage you want to add.

___________________________________________________________________________________________________
# USEFUL RESOURCES

## [Request] Player model tutorial (and modding questions) 
>   http://tesseract.gg/forum/viewtopic.php?id=288

## Trouble importing custom model
>   http://tesseract.gg/forum/viewtopic.php?id=234

## ALUI mod [ Alternate User Interface ]
>   http://tesseract.gg/forum/viewtopic.php?id=395

>   https://github.com/coornio/ALUI/tree/master

## How to create MASK texture ?
>   https://docs.unrealengine.com/latest/IN … o/Masking/

## Playermodel
>   http://tesseract.gg/forum/viewtopic.php?id=81
___________________________________________________________________________________________________